#!/bin/bash

. ~/slurm_jobs/2020/09/16/compile_and_run.sh \
/scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/2d/src \
"Fortran record length test with irecl_float=1 \
    https://gitlab.com/evgenyneu/honours_project/-/tree/master/a2020/a09/a16_reading_prompi_output#output-size-problem \
    https://github.com/evgenyneu/PROMPI/commit/64d95988adc48a0740aefc74e4d014e956f1ed8a" \
fortran_record_byte_rec_len
