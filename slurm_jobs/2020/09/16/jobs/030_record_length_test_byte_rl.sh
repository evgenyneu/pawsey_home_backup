#!/bin/bash

. ~/slurm_jobs/2020/09/16/compile_and_run.sh \
/scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/2d/src \
"Fortran record length test with byte recorl length compile flag -assume byterecl \
    https://gitlab.com/evgenyneu/honours_project/-/tree/master/a2020/a09/a16_reading_prompi_output#output-size-problem \
    https://github.com/evgenyneu/PROMPI/commit/eaa833fc71e5fc5e0889596b29891c9b4c41a16e" \
fortran_record_byte_rec_len
