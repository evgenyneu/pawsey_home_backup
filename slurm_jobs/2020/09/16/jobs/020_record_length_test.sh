#!/bin/bash

. ~/slurm_jobs/2020/09/16/compile_and_run.sh \
/scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/2d/src \
"Fortran record length test with default settings \
https://gitlab.com/evgenyneu/honours_project/-/tree/master/a2020/a09/a16_reading_prompi_output#output-size-problem \
https://github.com/evgenyneu/PROMPI/commit/a85041f0eb5c0e054595870a4a60989da27001ca" \
fortran_record_length_default
