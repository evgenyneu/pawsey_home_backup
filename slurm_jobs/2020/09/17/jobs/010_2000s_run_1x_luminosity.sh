#!/bin/bash

. ~/slurm_jobs/2020/09/16/compile_and_run.sh \
/scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/2d/src \
"2000s run with 1x luminocity \
https://gitlab.com/evgenyneu/honours_project/-/tree/master/a2020/a09/a17_reading_prompi_output#run-prompi" \
192x192x1_2000s_time_1.00_luminosity
