#!/bin/bash -l

#
# SLURM job that moves output of PROMPI simulation to safe location in
# $SCRATCH and $MYGROUP directories, for example:
#
#   * /scratch/ew6/evgenyneu/prompi_output/2020_09_15_15_52_02_[suffix]
#   * /group/ew6/evgenyneu/prompi_output/2020_09_15_15_52_02_[suffix].tar,
#
# where [suffix] is the first parameter supplied to this script
#
# Parameters
# ----------
#
#    $1: the suffix of the destination directory name.
#
# Example
# -------------
#
#    sbatch run_prompi.sh lovely_simulation
#

#SBATCH --job-name=copy_prompi
#SBATCH --account=ew6
#SBATCH --partition=copyq
#SBATCH --cluster=zeus
#SBATCH --time=00:30:00
#SBATCH --ntasks=1
#SBATCH --output=/home/evgenyneu/slurm_jobs/output/move_files_%j.out
#SBATCH --export=NONE
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sausageskin@gmail.com

. /home/evgenyneu/script/move_prompi_output.sh "$@"
