#!/bin/bash -l

#
# Compile and run PROMPI simulation.
#
# Parameters
# ----------
#
#    $1: path to the PROMPI setup source code
#
# Example
# -------------
#
#    ./compile_and_run.sh /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/2d/src
#

if [ -z "$1" ]; then
  echo "Source directory is missing"
  exit 1
fi

src_dir=$1
script_dir=/home/evgenyneu/slurm_jobs/2020/09/15
jobid=`sbatch $script_dir/compile_prompi.sh $src_dir | cut -d " " -f 4`
echo "Submitted compilation batch job ${jobid}"
echo "View job's status: sacct -j ${jobid}"
sbatch --dependency=afterok:$jobid $script_dir/run_prompi.sh $src_dir
