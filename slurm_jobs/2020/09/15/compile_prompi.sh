#!/bin/bash -l

#
# SLURM job that compiles PROMPI program.
#
# Parameters
# ----------
#
#    $1: path to the PROMPI setup source code
#
# Example
# -------------
#
#    sbatch compile_prompi.sh /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/2d/src
#

#SBATCH --job-name=build_prompi
#SBATCH --account=ew6
#SBATCH --partition=workq
#SBATCH --time=00:10:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --output=/home/evgenyneu/slurm_jobs/output/compile_prompi_%j.out
#SBATCH --export=NONE
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sausageskin@gmail.com

printf '\nBuild PROMPI'
printf '\n--------\n\n'
cat $0

# Use Intel compiler
module swap PrgEnv-cray PrgEnv-intel

printf '\n\nList of modules'
printf '\n--------\n\n'
module list 2>&1

printf '\n\nCurrent job'
printf '\n--------\n\n'
scontrol show job $SLURM_JOBID

if [ -z "$1" ]; then
  echo "Source directory is missing"
  exit 1
fi

# Change working directory
cd $1
make clean

# Set path to output file
date=$(date '+%Y_%m_%d_%H_%M_%S')
output_dir=/scratch/ew6/evgenyneu/jobs_output
mkdir -p $output_dir
output_file="${output_dir}/${date}_${SLURM_JOB_NAME}_%j.log"

# Run the program
printf '\n\nCurrent job output'
printf '\n--------\n\n'
echo $output_file
srun --export=ALL --output="${output_file}" make prompi

printf '\n\nRunning time'
printf '\n--------\n\n'
sacct -j $SLURM_JOBID -o jobid%20,Start,elapsed
