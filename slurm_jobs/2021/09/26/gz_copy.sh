#!/bin/bash -l

#
# SLURM job that copies output from PROMPI simulation to a direcotory.
# It compresses individual binary files, instead of entire data set.
# For example, it will create
# sim.00001.bindata.tag.gz, sim.00002.bindata.tag.gz in desination directory.
#
# Parameters
# ----------
#
#    $1: Source directory that contains *.bindata and *.header files.
#
#    $2: Destination directory.
#
# Usage
# -----
#
#     sbatch gz_copy.sh <source dir> <dest dir>
#
# Example
# -------------
#
#    sbatch gz_copy.sh /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d /group/ew6/evgenyneu/prompi_output/2021_09_18_3D
#

#SBATCH --job-name=gz_copy
#SBATCH --account=ew6
#SBATCH --partition=copyq
#SBATCH --mem=8GB
#SBATCH --cluster=zeus
#SBATCH --time=20:00:00
#SBATCH --ntasks=1
#SBATCH --output=/home/evgenyneu/slurm_jobs/output/gz_copy_%j.out
#SBATCH --export=NONE
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sausageskin@gmail.com

scr_dir=/home/evgenyneu/slurm_jobs/2021/09/26/

# Check paramters
# -------

if [ -z "$1" ]; then
  echo "Source directory is missing."
  echo "./gz_copy.sh <source dir> <dest dir>"
  exit 1
fi

src_dir=$1

if [ -z "$2" ]; then
  echo "Destination directory is missing."
  echo "./gz_copy.sh <source dir> <dest dir>"
  exit 1
fi

dest_dir=$2

# Run Python program
# -------

module load python/3.6.3
python $scr_dir/gz_copy.py "$src_dir" "$dest_dir"

