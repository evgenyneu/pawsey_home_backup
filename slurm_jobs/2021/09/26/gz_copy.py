#
# Copies PROMPI output to a directory, compressing individual bindata files
# for easier download.
#
# Usage example
# ----------
#
# python gz_copy.py /your_prompi_output_dir /your_dest_dir
#

import argparse
import subprocess
import os

def parse_command_line_args():
    parser = argparse.ArgumentParser(
        description=(
            'Copies PROMPI output from source to destination. '
            'It compresses individual *.bindata files before copying.'
        )
    )

    parser.add_argument(
        'src_dir',
        help='Directory containing PROMPI files to copy'
    )

    parser.add_argument('dest_dir', help='Destination directory')
    return parser.parse_args()


def compress(file_path):
    cmd = f"gzip --keep --force '{file_path}'"
    subprocess.run(cmd, shell=True, check=True)


def compress_files_with_suffix(suffix, src_dir):
    file_names = [
        f.name for f in os.scandir(src_dir)
        if f.name.endswith(suffix)
    ]

    for file_name in file_names:
        file_path = os.path.join(src_dir, file_name)
        compress(file_path)


def move_files_with_extension(extension, src_dir, dest_dir, check=True):
    cmd = f"mv {src_dir}/*{extension} {dest_dir}"
    subprocess.run(cmd, shell=True, check=check)


def copy_files_with_extension(extension, src_dir, dest_dir, check=True):
    cmd = f"cp {src_dir}/*{extension} {dest_dir}"
    subprocess.run(cmd, shell=True, check=check)


def move_files(src_dir, dest_dir):
    os.makedirs(dest_dir, exist_ok=True)

    compress_files_with_suffix(suffix='.bindata', src_dir=src_dir)

    move_files_with_extension(
        extension='.bindata.gz', src_dir=src_dir, dest_dir=dest_dir
    )

    copy_files_with_extension(
        extension='.header', src_dir=src_dir, dest_dir=dest_dir
    )

    compress_files_with_suffix(suffix='no_time8_function_', src_dir=src_dir)

    copy_files_with_extension(
        extension='README.md', src_dir=src_dir, dest_dir=dest_dir, check=False
    )


def lets_goooooo():
    args = parse_command_line_args()
    move_files(src_dir=args.src_dir, dest_dir=args.dest_dir)
    print(f"Success: copied PROMPI output to {args.dest_dir}")


if __name__ == '__main__':
    lets_goooooo()
    print('We are done!')




