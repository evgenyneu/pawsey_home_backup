#
# Extracts *.bindata.gz files in the given directory.
#
# Usage example
# ----------
#
# python extract_bindata.py /path_to_your_dir
#

import argparse
import subprocess
import os

def parse_command_line_args():
    parser = argparse.ArgumentParser(
        description=(
            'Extracts *.bindata.gz files in the given directory'
        )
    )

    parser.add_argument(
        'src_dir',
        help='Directory containing *.bindata.gz files.'
    )
    
    return parser.parse_args()


def uncompress(file_path):
    cmd = f"gzip -d '{file_path}'"
    subprocess.run(cmd, shell=True, check=True)


def uncompress_files_with_suffix(suffix, src_dir):
    file_names = [
        f.name for f in os.scandir(src_dir)
        if f.name.endswith(suffix)
    ]
    
    for file_name in file_names:
        file_path = os.path.join(src_dir, file_name)
        uncompress(file_path)
          
    
def extract_files(src_dir):
    uncompress_files_with_suffix(suffix='bindata.gz', src_dir=src_dir)
   
    
def lets_goooooo():
    args = parse_command_line_args()
    extract_files(args.src_dir)
    print(f"Success: extracted files in {args.src_dir}")


if __name__ == '__main__':
    lets_goooooo()
    print('We are done!')




