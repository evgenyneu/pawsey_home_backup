#!/bin/bash -l

#
# SLURM job that runs PROMPI program.
#
# Parameters
# ----------
#
#    $1: path to the PROMPI setup source code.
#
#    $2: A job description that will be saved in README.md file
#        of the output directory.
#
#    $3: Suffix of the destination directory name.
#
# Example
# -------------
#
#    sbatch run_prompi.sh /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d/src "This is a simulation of my lovely horse" lovely_simulation
#

#SBATCH --account=ew6
#SBATCH --partition=workq
#SBATCH --ntasks-per-node=24
#SBATCH --output=/home/evgenyneu/slurm_jobs/output/run_prompi_%j.out
#SBATCH --export=NONE
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sausageskin@gmail.com

printf '\nRun PROMPI'
printf '\n--------\n\n'
cat $0

# Use Intel compiler
module swap PrgEnv-cray PrgEnv-intel

printf '\n\nList of modules'
printf '\n--------\n\n'
module list 2>&1

printf '\n\nCurrent job'
printf '\n--------\n\n'
scontrol show job $SLURM_JOBID

if [ -z "$1" ]; then
  echo "Source directory is missing"
  exit 1
fi

src_dir=$1

# Change working directory
cd $src_dir
cd ..

if [ -z "$2" ]; then
  echo "Description is missing"
  echo "program <path to source> <job description> <output folder suffix>"
  exit 1
fi

job_description=$2

if [ -z "$3" ]; then
  echo "Output folder suffix is missing"
  echo "program <path to source> <job description> <output folder suffix>"
  exit 1
fi

output_dir_suffix=$3

# Set path to output file
date=$(date '+%Y_%m_%d_%H_%M_%S')
output_dir=/scratch/ew6/evgenyneu/jobs_output
mkdir -p $output_dir
output_file="${output_dir}/${date}_${SLURM_JOB_NAME}_%j.log"

# Run the program
printf '\n\nCurrent job output'
printf '\n--------\n\n'
echo $output_file
srun --export=ALL --output="${output_file}" ./prompi.x

# Copy output files
sbatch /home/evgenyneu/slurm_jobs/2021/09/16/run/move_files.sh "${src_dir}" "${job_description}" "${output_dir_suffix}"

printf '\n\nRunning time'
printf '\n--------\n\n'
sacct -j $SLURM_JOBID -o jobid%20,Start,elapsed
