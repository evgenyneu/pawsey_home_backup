#!/bin/bash -l

#
# SLURM job that moves output of PROMPI simulation to safe location in
# $SCRATCH and $MYGROUP directories, for example:
#
#   * /scratch/ew6/evgenyneu/prompi_output/2020_09_15_15_52_02_[suffix]
#   * /group/ew6/evgenyneu/prompi_output/2020_09_15_15_52_02_[suffix].tar,
#
# where [suffix] is the first parameter supplied to this script
#
# Parameters
# ----------
#
#    $1: path to the PROMPI setup source code.
#
#    $2: A job description that will be saved in README.md file
#        of the output directory.
#
#    $3: Suffix of the destination directory name.
#
# Usage
# -----
#
#   sbatch mote_files.sh <path to source> <job description> <output folder suffix>
#
# Example
# -------------
#
#    sbatch move_files.sh /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d/src "This is a simulation of my lovely horse" lovely_simulation
#

#SBATCH --job-name=copy_prompi
#SBATCH --account=ew6
#SBATCH --partition=copyq
#SBATCH --cluster=zeus
#SBATCH --time=00:30:00
#SBATCH --ntasks=1
#SBATCH --output=/home/evgenyneu/slurm_jobs/output/move_files_%j.out
#SBATCH --export=NONE
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sausageskin@gmail.com

if [ -z "$1" ]; then
  echo "Source directory is missing."
  echo "./move_files.sh <path to source> <job description> <output folder suffix>"
  exit 1
fi

src_dir=$1

# Change working directory
cd $src_dir
cd ..

if [ -z "$2" ]; then
  echo "Description is missing"
  echo "./move_files.sh <path to source> <job description> <output folder suffix>"
  exit 1
fi

job_description=$2

if [ -z "$3" ]; then
  echo "Output folder suffix is missing"
  echo "./move_files.sh <path to source> <job description> <output folder suffix>"
  exit 1
fi

suffix="_$3"

date=$(date '+%Y_%m_%d_%H_%M_%S')
output_dir="${MYSCRATCH}/prompi_output/${date}${suffix}"
mkdir -p $output_dir

mv *.bindata *.header *.ransdat *.ranshead run_complete_file $output_dir 2>/dev/null
echo "Copied output to ${output_dir}"

# Create a README file with the job description
echo $job_description > "${output_dir}/README.md"

# Archive to a group directory
archive_dir="${MYGROUP}/prompi_output"
mkdir -p $archive_dir
archive_path="${archive_dir}/${date}${suffix}.tar.gz"
tar -czf $archive_path -C $output_dir .
echo "Archived output to ${archive_path}"
