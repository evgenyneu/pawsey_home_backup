#!/bin/bash

. ~/slurm_jobs/2021/09/08/16_nodes/compile_and_run.sh \
/scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d/src \
"3D 200s run with 1x luminocity \
https://gitlab.com/evgenyneu/honours_project/-/tree/master/a2021/a09/a09_short_test_3d_simulation" \
192x192x192_200s_time_1.00_luminosity

