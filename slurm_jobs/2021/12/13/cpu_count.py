#
# Returns the number of CPUs
#

import multiprocessing


def lets_goooooo():
    cpus = multiprocessing.cpu_count()
    print('Number of CPUs: %d' % (cpus))


if __name__ == '__main__':
    lets_goooooo()
    print('We are done!')




