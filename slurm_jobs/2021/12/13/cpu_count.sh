#!/bin/bash -l

#
# Logs the number of CPUs
#
# Usage
# -----
#
#     sbatch cpu_count.sh
#

#SBATCH --job-name=cpu_count
#SBATCH --account=ew6
#SBATCH --partition=copyq
#SBATCH --mem=8GB
#SBATCH --cluster=zeus
#SBATCH --time=20:00:00
#SBATCH --ntasks=1
#SBATCH --output=/home/evgenyneu/slurm_jobs/output/cpu_count_%j.out
#SBATCH --export=NONE
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sausageskin@gmail.com

scr_dir=/home/evgenyneu/slurm_jobs/2021/12/13/

# Run Python program
# -------

# module load python/3.6.3
python $scr_dir/cpu_count.py
