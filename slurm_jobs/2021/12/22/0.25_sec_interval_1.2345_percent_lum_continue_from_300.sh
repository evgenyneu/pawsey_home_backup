#!/bin/bash

. ~/slurm_jobs/2021/11/05/run/compile_and_run.sh \
/scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d/src \
"0.25 sec interval luminocity 1.2345 percent continue from dump 300   \
https://gitlab.com/evgenyneu/honours_project/-/tree/master/a2021/a12/a22_run_264_2d_with_0.25_sec_framerate" \
3d_0_25_sec_interval_continue_dump_300_lum_1_2345_percent \
36 \
"24:00:00" \
6a52256

