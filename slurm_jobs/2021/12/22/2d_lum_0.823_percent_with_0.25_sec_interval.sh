#!/bin/bash

. ~/slurm_jobs/2021/09/16/run/compile_and_run.sh \
/scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/2d/src \
"2D luminosity 0.823 percent with 0.25 sec interval \
https://gitlab.com/evgenyneu/honours_project/-/tree/master/a2021/a12/a22_run_264_2d_with_0.25_sec_framerate" \
2d_lum_0_823_percent_with_0_25_sec_interval \
3 \
"5:00:00" \
6a52256

