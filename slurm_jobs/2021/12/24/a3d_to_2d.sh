#!/bin/bash -l

#
# SLURM job that converts 3D data into 2D
#
# Parameters
# ----------
#
#    $1: Source directory that contains *.bindata and *.header files for 3D simulation
#
#    $2: Destination directory where converted files will be created.
#
#    $3: Z-coordinate that will be read in 3D files.
#
#    $4: Maximum number of cores to be used.
#
# Usage
# -----
#
#     sbatch a3d_to_2d.sh <source dir> <dest dir> <zindex> <cores>
#
# Example
# -------------
#
#    sbatch a3d_to_2d.sh /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d /group/ew6/evgenyneu/prompi_output/2021_09_18_3D 50
#

#SBATCH --job-name=3d_to_2d
#SBATCH --account=ew6
#SBATCH --partition=copyq
#SBATCH --mem=64GB
#SBATCH --cluster=zeus
#SBATCH --time=01:00:00
#SBATCH --ntasks=1
#SBATCH --output=/home/evgenyneu/slurm_jobs/output/3d_to_2d_%j.out
#SBATCH --export=NONE
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sausageskin@gmail.com

code_dir=/home/evgenyneu/slurm_jobs/2021/12/24/thylacine

# Check paramters
# -------

if [ -z "$1" ]; then
  echo "Source directory is missing."
  echo "./a3d_to_2d.sh <source dir> <dest dir> <zindex> <cores>"
  exit 1
fi

src_dir=$1

if [ -z "$2" ]; then
  echo "Destination directory is missing."
  echo "./a3d_to_2d.sh <source dir> <dest dir> <zindex> <cores>"
  exit 1
fi

dest_dir=$2

if [ -z "$3" ]; then
  echo "Z-index is missing"
  echo "./a3d_to_2d.sh <source dir> <dest dir> <zindex> <cores>"
  exit 1
fi

zindex=$3

if [ -z "$4" ]; then
  echo "Number of codes"
  echo "./a3d_to_2d.sh <source dir> <dest dir> <zindex> <cores>"
  exit 1
fi

cores=$4

# Run Python program
# -------

module load python/3.6.3
module load numpy/1.19.0
python $code_dir/convert_3d_to_2d_cmd.py "$src_dir" "$dest_dir" $zindex $cores


