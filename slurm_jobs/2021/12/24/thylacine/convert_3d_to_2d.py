import os
import re
import glob
import numpy as np
import multiprocessing
from multiprocessing import Pool

from reader import (
    get_variable_data_2d,
    get_grid_size,
    get_variable_names,
    get_header_path,
    parse_epoch
)

def convert_bindata_file(input_path, output_path, zindex):
    """Converts bindata files from 3D into 2D using one z-slice at `zindex`"""
    variables = get_variable_names(input_path)
    variables.remove('velz')
    data_out = None

    for i, variable in enumerate(variables):
        data = get_variable_data_2d(data_path=input_path, variable=variable, zindex=zindex)
        pixels = data.shape[0] * data.shape[1]

        if data_out is None:
            shape = pixels * len(variables)
            data_out = np.empty(shape=shape, order='F')

        data_out[i * pixels : (i + 1) * pixels] = data.flatten(order='F')

    dtype = '<f4'  # little-endian 32-bit floating-point number
    data_out.astype(dtype).tofile(output_path)


def change_second_line(data_path, line):
    """
    Changes the second line of the header file to make it describe 2D data:
        1. Changes z dimension to 1.
        2. Decreases the number of variables by 1 (2D does not have 'velz').
    """
    nx, ny, nz = get_grid_size(data_path)
    variables = get_variable_names(data_path)
    nvariables = len(variables)

    return re.sub(fr'(\s+{nx}\s+{ny}\s+){nz}(\s+2\s+){nvariables}',
                  fr'\g<1>1\g<2>{nvariables - 1}',
                  line)


def remove_velz(lines):
    """Removes z velocity from header file lines"""
    return [line for line in lines if not re.match(r'\s*velz\s*$', line)]


def write_header(output_path, lines):
    """Saves array of lines from header file into `output_path`"""
    output_header = get_header_path(output_path)

    with open(output_header, 'w') as file:
        file.writelines(lines)


def convert_header_file(input_path, output_path):
    """Convert header file from 3D to 2D"""
    input_header = get_header_path(input_path)
    lines = []

    with open(input_header) as file:
        lines = file.readlines()

    lines[1] = change_second_line(input_path, lines[1])
    lines_2d = remove_velz(lines)
    if len(lines_2d) != len(lines) - 1: raise Exception("Can't find velz")
    write_header(output_path, lines_2d)


def convert(input_path, output_path, zindex):
    """
    Reads one z-slice (`zindex`) from 3D file (`input_path`)
    and converts it to a 2D file located at `output_path`.
    """
    convert_bindata_file(input_path, output_path, zindex)
    convert_header_file(input_path, output_path)


def convert_params(params):
    convert(**params)


def output_file_name(path_3d, output_dir, basename, number_length):
    """
    Constructs path to output 2D file given path to 3D.

    Parameters
    ----------

    path_3d : string
        Path to 3D bindata file.

    output_dir : string
        Directory where 2D file will be created.

    basename : string
        Basename of the output 2D file. For example,
        '2d.convection.00023.bindata' has basename of '2d.convection.'

    number_length : int
        Length of the numeric part of the output file name. For example,
        it is 5 in '2d.00023.bindata' because "00023" part has 5 characters.
    """
    epoch = parse_epoch(path_3d)
    filename = str(epoch).rjust(number_length, '0')
    filename = f"{basename}{filename}.bindata"
    return os.path.join(output_dir, filename)


def convert_dir(input_dir, output_dir, basename, number_length, zindex,
                max_processes=1000):
    """
    Convert all files in `input_dir` from 3D to 2D by
    reading one z-slice at `zindex`.

    Parameters
    ----------

    input_dir : string
        Path to direcotry containing 3D data.

    output_dir : string
        Directory where 2D file will be created.

    basename : string
        Basename of the output 2D file. For example,
        '2d.convection.00023.bindata' has basename of '2d.convection.'

    number_length : int
        Length of the numeric part of the output file name. For example,
        it is 5 in '2d.00023.bindata' because "00023" part has 5 characters.

    zindex : int
        Z-coordinate that will be read from 3D files.

    max_processes : Maximum number of processes to use.
    """

    os.makedirs(output_dir, exist_ok=True)
    pattern = os.path.join(input_dir, '*.bindata')
    files = list(glob.glob(pattern))
    files = sorted(files)

    params = [
        {
            'input_path': file,
            'output_path': output_file_name(
                path_3d=file, output_dir=output_dir, basename=basename,
                number_length=number_length
            ),
            'zindex': zindex
        }
        for file in files
    ]

    processes = multiprocessing.cpu_count()
    if processes > max_processes: processes = max_processes

    with Pool(processes=processes) as p:
        list(p.imap(convert_params, params))

