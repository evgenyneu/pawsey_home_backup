
import os
import shutil
from pytest import approx
from fixtures import data_dir3d, data_path3d, this_dir
from convert_3d_to_2d import convert, convert_dir

from reader import (
    get_variable_data, get_grid_size, get_variable_count, get_time
)


def test_convert(data_path3d):
    output_dir = os.path.join(this_dir(), 'test_output')
    os.makedirs(output_dir, exist_ok=True)
    output_file = os.path.join(output_dir, '2d.bindata')

    convert(
        input_path=data_path3d,
        output_path=output_file,
        zindex=0
    )

    data = get_variable_data(data_path=output_file, variable='0001')

    assert data.shape == (16, 16, 1)
    assert data[0, 0, 0] == approx(0.85845196, rel=1e-7)
    assert data[15, 15, 0] == approx(0.7153248, rel=1e-7)
    assert data[12, 12, 0] == approx(0.49849552, rel=1e-7)

    assert get_grid_size(output_file) == (16, 16, 1)
    assert get_variable_count(output_file) == 12
    assert get_time(output_file) == 25.091475

    shutil.rmtree(output_dir)


def test_convert_dir(data_dir3d):
    output_dir = os.path.join(this_dir(), 'test_output')
    os.makedirs(output_dir, exist_ok=True)

    convert_dir(input_dir=data_dir3d, output_dir=output_dir,
                basename='ccptwo.2D.', number_length=5, zindex=4,
                max_processes=3)

    output_file = os.path.join(output_dir, 'ccptwo.2D.00005.bindata')

    data = get_variable_data(data_path=output_file, variable='0001')

    assert data.shape == (16, 16, 1)
    assert data[0, 0, 0] == approx(0.9999567, rel=1e-7)
    assert data[15, 15, 0] == approx(0.2742604, rel=1e-7)
    assert data[12, 12, 0] == approx(0.7427635, rel=1e-7)

    assert get_grid_size(output_file) == (16, 16, 1)
    assert get_variable_count(output_file) == 12
    assert get_time(output_file) == 20.075218

    shutil.rmtree(output_dir)
