from reader import (
    get_variable_data, get_variable_data_2d, get_variable_data_1d,
    get_variable_names, get_variable_count, get_grid_size,
    get_time, locate_epochs)

from timeit import default_timer as timer
import numpy as np
from pytest import approx
from fixtures import data_dir3d, data_path3d


def test_get_variable_data(data_path3d):
    data = get_variable_data(data_path=data_path3d, variable='0001')

    assert data.shape == (16, 16, 16)
    assert data[0, 0, 0] == approx(0.85845196, rel=1e-7)
    assert data[15, 15, 0] == approx(0.7153248, rel=1e-7)
    assert data[12, 12, 0] == approx(0.49849552, rel=1e-7)


def test_get_variable_data_performance(data_path3d):
    variables = [
        "density", "velx", "vely", "energy", "press", "temp",
        "gam1", "gam2", "enuc1", "enuc2", "0001", "0002"]

    runtimes = []

    for variable_name in variables:
        start = timer()
        get_variable_data(data_path=data_path3d, variable=variable_name)
        end = timer()
        runtimes.append(end - start)

    assert np.median(runtimes) < 0.001


def test_get_variable_data_2d(data_path3d):
    data = get_variable_data_2d(data_path=data_path3d, variable='0001', zindex=0)

    assert data.shape == (16, 16)
    assert data[0, 0] == approx(0.85845196, rel=1e-7)
    assert data[11, 12] == approx(0.52956074, rel=1e-7)
    assert data[13, 14] == approx(0.57341146, rel=1e-7)


def test_get_variable_data_2d_performance(data_path3d):
    variables = [
        "density", "velx", "vely", "energy", "press", "temp",
        "gam1", "gam2", "enuc1", "enuc2", "0001", "0002"]

    runtimes = []

    for variable_name in variables:
        start = timer()
        get_variable_data_2d(data_path=data_path3d, variable=variable_name, zindex=0)
        end = timer()
        runtimes.append(end - start)

    assert np.median(runtimes) < 0.0001


def test_get_variable_data_1d(data_path3d):
    data = get_variable_data_1d(data_path=data_path3d, variable='0001')

    assert data.shape == (16, )
    assert data[0] == approx(0.85845196, rel=1e-7)
    assert data[10] == approx(0.65967304, rel=1e-7)
    assert data[15] == approx(0.7412559, rel=1e-7)


def test_get_variable_names(data_path3d):
    names = get_variable_names(data_path3d)

    assert len(names) == 13

    assert names[0] == 'density'
    assert names[1] == 'velx'
    assert names[11] == '0001'


def test_get_get_variable_count(data_path3d):
    result = get_variable_count(data_path3d)
    assert result == 13


def test_get_grid_size(data_path3d):
    x, y, z = get_grid_size(data_path3d)

    assert x == 16
    assert y == 16
    assert z == 16


def test_get_time(data_path3d):
    result = get_time(data_path3d)

    assert result == 25.091475


def test_locate_epochs(data_dir3d):
    result = locate_epochs(data_dir3d)
    assert len(result) == 6
    assert result[0] == 1
    assert result[5] == 6
