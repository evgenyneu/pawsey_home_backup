# Read PROMPI output data files

import numpy as np
import os
import glob
import re

# Name of the first variable in the header file
FIRST_VARIABLE_NAME = "density"


def get_variable_data(data_path, variable):
    """Return data for the variable from PROMPI data file.

    Parameters
    ----------
    data_path : str
        Path to PROMPI data file.
    variable : str
        A name of the variable, example: 'density' or 'energy'.

    Returns
    -------
    numpy.ndarray
        An array containing data for the variable

    """

    nx, ny, nz = get_grid_size(data_path)
    variable_names = get_variable_names(data_path)
    numbers = nx * ny * nz  # The number of data values for the variable

    # Offset in bytes of the variable block in file
    offset = variable_names.index(variable) * numbers * 4
    dtype = '<f4'  # little-endian 32-bit floating-point number

    # Read data to 1D array
    data = np.fromfile(data_path, dtype=dtype, count=numbers, offset=offset)

    # Convert to 3D array
    data = np.reshape(data, (nx, ny, nz), order='F')

    return data


def get_variable_data_2d(data_path, variable, zindex):
    """Return 2D slice from a 3D cube for the variable from PROMPI data file.

    Parameters
    ----------
    data_path : str
        Path to PROMPI data file.
    variable : str
        A name of the variable, example: 'density' or 'energy'.
    zindex : z value for the 2D slice.

    Returns
    -------
    numpy.ndarray
        A 2D array containing data for the variable.
    """

    nx, ny, nz = get_grid_size(data_path)
    variable_names = get_variable_names(data_path)

    # Offset in bytes of the variable block in file
    offset = variable_names.index(variable) * nx * ny * nz * 4
    offset += nx * ny * 4 * zindex # Add offset for 2D slice for the variable
    dtype = '<f4'  # little-endian 32-bit floating-point number

    # Read data to 1D array
    data = np.fromfile(data_path, dtype=dtype, count=nx * ny, offset=offset)

    # Convert to 2D array
    data = np.reshape(data, (nx, ny), order='F')

    return data


def get_variable_data_1d(data_path, variable):
    """Return 2D slice from a 3D cube for the variable from PROMPI data file.

    Parameters
    ----------
    data_path : str
        Path to PROMPI data file.
    variable : str
        A name of the variable, example: 'density' or 'energy'.

    Returns
    -------
    numpy.ndarray
        A 1D array containing data for the variable.
    """

    nx, ny, nz = get_grid_size(data_path)
    variable_names = get_variable_names(data_path)

    # Offset in bytes of the variable block in file
    offset = variable_names.index(variable) * nx * ny * nz * 4
    dtype = '<f4'  # little-endian 32-bit floating-point number

    # Read data to 1D array
    data = np.fromfile(data_path, dtype=dtype, count=nx, offset=offset)

    # Convert to 2D array
    data = np.reshape(data, (nx), order='F')

    return data


def get_header_path(data_path):
    """Return path to the header file.

    Parameters
    ----------
    data_path : str
        Path to PROMPI data file.

    Returns
    -------
    str
        Path to header file

    """

    return data_path.replace(".bindata", ".header")


def get_variable_names(data_path):
    """Return the names of all variables in the data file.

    Parameters
    ----------
    data_path : str
        Path to PROMPI data file.

    Returns
    -------
    list of str
        Names of all variables in the data file.

    """

    header_path = get_header_path(data_path)
    variable_count = get_variable_count(data_path)

    names = []
    firt_var_index = None

    with open(header_path) as file:
        for line_index, line in enumerate(file):
            if firt_var_index is None:
                # Find the linke index of the first variable
                if FIRST_VARIABLE_NAME == line.strip():
                    firt_var_index = line_index

            if firt_var_index is not None:
                names.append(line.strip())

                if line_index > firt_var_index + variable_count - 2:
                    return names


def get_variable_count(data_path):
    """Return the number of variables.

    Parameters
    ----------
    data_path : str
        Path to PROMPI data file.

    Returns
    -------
    int
        Number of variables.

    """

    header_path = get_header_path(data_path)

    with open(header_path) as file:
        file.readline()
        second_line = file.readline()
        return int(second_line.split()[4])


def get_grid_size(data_path):
    """Return the number of grid points in x, y and z directions.

    Parameters
    ----------
    data_path : str
        Path to PROMPI data file.

    Returns
    -------
    (int, int, int)
        Number of grid points (x, y, z).

    """

    header_path = get_header_path(data_path)

    with open(header_path) as file:
        file.readline()
        second_line = file.readline()
        splitted = second_line.split()
        x = int(splitted[0])
        y = int(splitted[1])
        z = int(splitted[2])
        return x, y, z


def get_time(data_path):
    """Return the time value of the data file.

    Parameters
    ----------
    data_path : str
        Path to PROMPI data file.

    Returns
    -------
    float
        Time of the data file.
    """

    header_path = get_header_path(data_path)

    with open(header_path) as file:
        first_line = file.readline()
        splitted = first_line.split()
        return float(splitted[1])


def parse_epoch(bindata_path):
    result = re.search(r"\.(\d+)\.bindata", bindata_path)
    return int(result.group(1))


def locate_epochs(dir):
    """Return the list of epochs corresponding to all *.bindata files
    located in directory `dir`. For example 'ccptwo.3D.00101.bindata' file
    is epoch 101.

    Parameters
    ----------
    dir : str
        Directory where *.bindata files are located.

    Returns
    -------
    list of int
        List of epochs.
    """

    pattern = os.path.join(dir, f"*.bindata")
    epochs = [parse_epoch(file) for file in glob.glob(pattern)]
    epochs.sort()
    return epochs
