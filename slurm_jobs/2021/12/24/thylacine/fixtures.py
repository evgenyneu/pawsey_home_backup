import pytest
import inspect
import os

def this_dir():
    frame = inspect.stack()[1]
    module = inspect.getmodule(frame[0])
    codefile = module.__file__
    return os.path.dirname(codefile)

@pytest.fixture
def data_dir3d():
    return os.path.join(this_dir(), 'testdata')

@pytest.fixture
def data_path3d(data_dir3d):
    return os.path.join(data_dir3d, 'ccptwo.3D.00006.bindata')
