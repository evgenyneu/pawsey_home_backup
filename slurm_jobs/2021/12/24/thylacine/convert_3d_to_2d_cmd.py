#
# Converts PROMPI output from 3D to 2D.
#
# Usage example
# ----------
#
# python convert_3d_to_2d_cmd.py /your_prompi_3d-output_dir /your_dest_dir 100
#

import argparse
from convert_3d_to_2d import convert_dir

def parse_command_line_args():
    parser = argparse.ArgumentParser(
        description=(
            'Converts PROMPI output from 3D to 2D. '
        )
    )

    parser.add_argument(
        'input_dir',
        help='Directory containing PROMPI 3D ouput'
    )

    parser.add_argument(
        'output_dir',
        help='Destination directory where 2D files will be created'
    )

    parser.add_argument(
        'zindex',
        type=int,
        help='Z-coordinate that will be read from 3D files'
    )

    parser.add_argument(
        'max_processes',
        type=int,
        help='Maximum number of precessor cores to use'
    )

    return parser.parse_args()


def lets_goooooo():
    args = parse_command_line_args()

    convert_dir(input_dir=args.input_dir, output_dir=args.output_dir,
                basename='ccptwo.2D.', number_length=5, zindex=args.zindex,
                max_processes=args.max_processes)

    print(f"Success: 2D files saved to {args.output_dir}")


if __name__ == '__main__':
    lets_goooooo()
    print('We are done!')




