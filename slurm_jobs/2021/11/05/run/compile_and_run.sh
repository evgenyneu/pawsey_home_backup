#!/bin/bash -l

#
# Runs three SLURM jobs one after another:
#
#   1) Compile PROMPI
#   2) Run PROMPI
#   3) Copy PROMPI output to scratch and group directories.
#
# Parameters
# ----------
#
#    $1: path to the PROMPI setup source code.
#
#    $2: A job description that will be saved in README.md file
#        of the output directory.
#
#    $3: Suffix of the destination directory name.
#
#    $4: Number of nodes (each node has 24 cores on Magnus)
#
#    $5: Wall-clock time limit for the job, two hours is "02:00:00"
#
#    $6: Git commit hash for PROMPI to use for compilation.
#
# Usage
# ------
#
#   ./compile_and_run.sh <path to source> <job description> <output folder suffix> <nodes> <time> <commit>
#
# Example
# -------------
#
#   ./compile_and_run.sh /scratch/ew6/evgenyneu/PROMPI/setups/ccp_two_layers/3d/src "My loverly horse simulation" my_lovely_horse 3 "02:00:00" 81509c5
#

example="./compile_and_run.sh <path to source> <job description> <output folder suffix> <nodes> <time> <commit>"

if [ -z "$1" ]; then
  echo "Source directory is missing."
  echo $example
  exit 1
fi

src_dir=$1


if [ -z "$2" ]; then
  echo "Job description is missing."
  echo $example
  exit 1
fi

job_description=$2


if [ -z "$3" ]; then
  echo "Output folder suffix is missing."
  echo $example
  exit 1
fi

output_dir_suffix=$3


if [ -z "$4" ]; then
  echo "Number of nodes is missing."
  echo $example
  exit 1
fi

nodes=$4


if [ -z "$5" ]; then
  echo "Wall time is missing"
  echo $example
  exit 1
fi

wall_time=$5


if [ -z "$6" ]; then
  echo "Git commit is missing"
  echo $example
  exit 1
fi

git_commit=$6

# Get dirrectory of this script file
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# 1. Compile PROMPI
# -------

jobid=`sbatch $script_dir/compile_prompi.sh "$src_dir" $git_commit | cut -d " " -f 4`

echo "Submitted PROMPI compilation job ${jobid}"
echo "View job's status: sacct -j ${jobid}"

# 2. Run PROMPI
# -------

jobid=`sbatch --job-name="$output_dir_suffix" --nodes=$nodes --time="$wall_time" --dependency=afterok:$jobid $script_dir/run_prompi.sh "${src_dir}" "${job_description}" "${output_dir_suffix}" | cut -d " " -f 4`

echo "Submitted PROMPI run job ${jobid}"
echo "View job's status: sacct -j ${jobid}"
