
PROGRAM prompi2hdf5_2D

!For converting PROMPI bindata files to HDF5+XMDF files for
!reading into VisIt or other plotting software.
!SWC OCT/2014: v0.6
!SWC DEC/2015: v0.8 Upgrading to handle massive data files.
!SWC JUL/2016: Continuing to upgrade for massive data files.
!              -Moved output choices to param file.
!              -Bugfix: Banding in h-averaged values was due to rounding error. Just changed sum in hav
!                      function to real*8.
!              -Note that Visit is slow at calculating derived vars, better to do it here.
!              -It seems Visit does not scale well.. also it is usually only set up to run on 1 node (on the 2
!                      supercomputers I've used so far). Haven't investigated batch/python jobs yet.
!              -Bugfix: Binary RECL can only be as large as 1.0e9.. oburn is 1.6e9. Changed bindat read in
!                      chunks to get around this.

!Random notes for way to deal with massive files:
! 1) Read in one 3D bindat var at a time (=> file size/20 memory required). Oburn vhr = 216GB/20 ~ 10 GB memory (biggest sim for a while?)
! 2) Read in one dimension worth of single var at a time => file size/20/dim_res RAM. Oburn ~ 10GB/1000 ~ 10 MB memory!
!However (2) changes the writing format for the HDF file, would need to write each var in 1D chunks to save RAM. Not sure if this makes
!for slower vis through complicated HDF files. (??)
!Biggest CHeB sim ~10 GB files? => 10/20 = 500MB RAM. Laptop has 8GB so not a problem until files ~160 GB. In which case the files wouldn't be on a laptop :P
!Went with option (1) for now, since viz machines have big RAM anyway (256 GB/node on Hydra).
!Binary RECL can only be as large as 1.0e9.. oburn is 1.6e9. Changed bindat read to chunks to get around this.

  USE HDF5

  IMPLICIT NONE

  real*8, parameter :: pi = dacos(-1.d0)
  integer(HSIZE_T) :: qx,qy,qz
  integer(HID_T) :: file_id, dset_id, dspace_id
  integer :: rank, error, altag
  integer*8 dims(2), dimx(1), dimy(1)
  integer*8 dimsx(1),dimsy(1)

  !Allocate big arrays after dims read in since simulation dims differ:
  real, allocatable :: bindat(:,:),bindat1(:,:),bindat2(:,:) !,vx(:,:,:),vy(:,:,:),vz(:,:,:)  !3D => huge! (file size/20) ~ size per var. Mres: 20*864*288*288*4 = 5.7 GB/20 ~300 MB.
  real, allocatable :: x(:),y(:),r(:)
  real, allocatable :: xx(:), yy(:)
  real, allocatable :: xznr(:),xznl(:),yznr(:),yznl(:)  !13 1D arrays. For mres ~400 on ave. real*4 => 4*400*13 = 20 MB.

  real*8 nstep, time
  real*8 dum3(6),dum4(16)
  real*4 hav, mint

  integer nmax,i,j,k,l,nnuc,nvar,outdim,len
  integer dumi,idat(60),ndat,nfiles,ii,vabs
  integer irecl,cnt, offset, ndattot, nextra
  integer nraw, nderiv, ivar, jj

  integer, parameter :: FILENAME_LENGTH = 1024

  character*30 flist
  character(len=FILENAME_LENGTH) filenames(5000),fbin
  character(len=FILENAME_LENGTH) fhead
  character(len=FILENAME_LENGTH) fxmf
  character(len=FILENAME_LENGTH) fhdf5
  character(len=FILENAME_LENGTH) basename
  integer :: basename_length
  character*12 dsetname, datname(60)
  character*5 varnames(60), varraw(60), varderiv(60)
  character dums


  altag = 0 !allocation tag

!---READ PARAM FILE-----
  flist ='param.txt' !A list of all the bindata filenames, and choices for HDF5 output data.

  open(12,file=flist,status='old')
  read(12,*)
  read(12,*) nfiles
  do ii=1,nfiles
     read(12,*) filenames(ii)
     print*,"filename ",ii,"  read: ", filenames(ii)
  enddo
  read(12,*)
  read(12,*) nraw
  do ii=1,nraw
     read(12,*) varraw(ii)
     print*,"Raw var requested = ",varraw(ii)
  enddo
  read(12,*)
  read(12,*) nderiv
  do ii=1,nderiv
     read(12,*) varderiv(ii)
     print*,"Derived var requested = ",varderiv(ii)
  enddo

!-------END READ PARAM FILE---

  do ii=1,nfiles !Over all files

     fbin = filenames(ii)

     !Deduce other filenames from bindata filename:
     basename_length = INDEX(fbin, '.bindata', .true.)
     print *, 'basename_length=',basename_length
     basename = fbin(1:basename_length)
     fhead = trim(basename)//'header'
     fhdf5 = trim(basename)//'h5'
     fxmf = trim(basename)//'xmf'
     print*,'Header file name=',fhead
     print*,'HDF5 file name=',fhdf5
     print*,'XMF file name=',fxmf

     !========READ HEADER:=================

     open(10,file=fhead,STATUS='OLD')
     print*,'Opened header file:',fhead
     read(10,*) nstep, time
     print*,'HEADER: nstep,time=',nstep,time
     read(10,*) qx,qy,qz,nnuc,nvar
     print*,' qx,qy,qz,nnuc,nvar=', qx,qy,qz,nnuc,nvar
     read(10,*) dum3!dummy line?
     read(10,*) dum4

     !Allocate arrays:
     if(altag.eq.0) then
        allocate(bindat(qx,qy))
        allocate(bindat1(qx/2,qy))
        allocate(bindat2(qx/2,qy))
        print*,'Allocated bindat array, size =',(qx*qy*4)/1e9, ' GB'
        print*,'Estimated size of your bindata file =',nvar*(qx*qy*4)/1e9, ' GB'
        allocate(r(qx+1),y(qy+1)) !radius, polar, azimuthal
        allocate(xznr(qx),xznl(qx),yznr(qy),yznl(qy))
        outdim = (qx+1)*(qy+1)
        allocate(xx(outdim),yy(outdim)) !Cartestian xyz ***THIS IS MAIN MEMORY HOG --> look at HDF5, can't we just give it xyz?
                                                   !--> YES :) (see prompi2hdf-spherical)
                                                   !--> BUT VisIt isn't plotting the raw data and converting to cartesian coords in Visit
                                                   !    takes ages... so back to cartesian output.
!        print*,'Allocated small arrays, total size =',(3*qx*4 + 3*qy*4 + 3*qz*4)/1e9, ' GB'
        print*,'Allocated cartesian coord arrays, total size = ', 3*qx*qy*4/1e9, ' GB'
        altag = 1
        print*,'TOTAL MEMORY REQUIRED = ',((3*qx*4 + 3*qy*4)+(3*qx*qy*4)+(qx*qy*4))/1e9,' GB'
     endif

     print*,'-----VARIABLE NAMES IN HEADER:----------'
     do i=1,nvar
        read(10,*) varnames(i)
        print*,varnames(i),i
     enddo
     print*,'----------------------------------------'

!     read(10,*)mint
!     print*,'mass interior to inner boundary = ',mint

     !Radii, cell centre.
     do i=1,qx
        read(10,101) dums,dumi,xznl(i),r(i),xznr(i)  !left, centre, right of zone. Radii.
        !print*,'r(i) = ', r(i),i
     enddo
     print*,'Innermost radius in binheader = ',r(1)
     print*,'Outermost radius in binheader = ',r(qx)
     print*

!(radial, polar, azimuthal) = (x,y,z) = (r,theta,phi) [= ISO standard]

     !y coord
     do i=1,qy
        read(10,101) dums,dumi,yznl(i),y(i),yznr(i)  !left, centre, right of zone. In radians.
        !NOTE y(i) = y(i) + 0.5d0*pi Since polar angle restricted to 0->pi
        !print*,'y(i) = ', y(i),i
     enddo
     print*

     !z coord
    ! do i=1,qz
    !    read(10,101) dums,dumi,zznl(i),z(i),zznr(i)  !left, centre, right of zone.
        !if(i.le.10.or.i.ge.qz-5.or.(i.ge.95.and.i.le.105)) write(6,103), 'i, z(i)=',i,z(i) !check
        !print*,'z(i) = ', z(i),i
    ! enddo

     !A quick hack- CHECK!
     print*,'***WARNING: Need to fix outer points (ie. qx,qy,qz +1)***'
     r(qx+1) = r(qx)
     y(qy+1) = y(qy)
     !z(qz+1) = z(qz)

     !Don't! Convert from spherical to cartesian coords:
     l=0
     !do i=1,qz+1 !Over azimuthal angle (z = phi)
        do j=1,qy+1 !Over polar angle (y = theta)
           do k=1,qx+1 !Over radii (x = r)
              l = l+1
              xx(l) = r(k)
              yy(l) = y(j)
              !zz(l) = r(i)
           enddo
        enddo
     !enddo
     print*,'NZONES =',l

101  format(a4,i4,3e15.8)
102  format(a,i10,i4,1P,3e17.8)
103  format(a,i4,1P,e15.8)
     close(10)
     print*,'Closed Prompi header file.'

     !=====CREATE & WRITE to HDF5 + XDMF file:===========================

     open(13,file=fxmf,status="replace") !Open XDMF file
     !Write XDMF header:
     write(13,105)'<?xml version="1.0" ?>'  !Format needed to avoid single space on left (VisIt doesn't like).
     write(13,*)'<!DOCTYPE Xdmf SYSTEM "Xdmf.dtd" []>'
     write(13,*)'<Xdmf Version="2.0">'
     write(13,*) ' <Domain>'
     write(13,*) '   <Grid Name="PrompiMesh" GridType="Uniform">'
     write(13,*) '           <Time Value="',time,'" />'
     write(13,104) '     <Topology TopologyType="2DSMesh" NumberOfElements="',qy+1,' ',qx+1,'"/>'

     !Start HDF5 API:
     CALL h5open_f(error) !Initialise Fortran HDF5
     CALL h5fcreate_f(fhdf5, H5F_ACC_TRUNC_F, file_id, error) !Create HDF5 FILE

     print*,'Created HDF5 file'

     !---Write coord arrays for mesh:---------

     !Write separate coordinate arrays for XYZ.
     !rank = dimensions of data space, ie. 1D/3D
     !dims = dimensions of dataset (eg. 200x200x400). Note ZYX order.
     rank = 2  ! Dataset rank (ie. 2D,3D)
     !dims(1) = qz+1
     dims(1) = qy+1
     dims(2) = qx+1

     !dimz = qz+1
     !dimy = qy+1
     !dimx = qx+1

     !-----x-coord array:---------------
     dsetname ='x'
     CALL h5screate_simple_f(2, dims, dspace_id, error) !Create data SPACE
     CALL h5dcreate_f(file_id, dsetname, H5T_NATIVE_REAL , dspace_id, dset_id, error) !Create data SET
     CALL h5dwrite_f(dset_id, H5T_NATIVE_REAL , xx, dims, error) !Write Data SET
     CALL h5dclose_f(dset_id, error) !Close DATASET
     CALL h5sclose_f(dspace_id, error) !Close data SPACE
     print*,'Wrote x data'

     !-----y-coord array:---------------
     dsetname ='y'
     CALL h5screate_simple_f(2, dims, dspace_id, error) !Create data SPACE
     CALL h5dcreate_f(file_id, dsetname, H5T_NATIVE_REAL , dspace_id, dset_id, error) !Create data SET
     CALL h5dwrite_f(dset_id, H5T_NATIVE_REAL , yy, dims, error) !Write DATA
     CALL h5dclose_f(dset_id, error) !Close DATASET
     CALL h5sclose_f(dspace_id, error) !Close data SPACE
     print*,'Wrote y data'

     !-----z-coord array:---------------
!     dsetname ='z'
!     CALL h5screate_simple_f(3, dims, dspace_id, error) !Create data SPACE
!     CALL h5dcreate_f(file_id, dsetname,  H5T_NATIVE_REAL , dspace_id, dset_id, error) !Create data SET
!     CALL h5dwrite_f(dset_id, H5T_NATIVE_REAL , zz, dims, error) !Write DATA
!     CALL h5dclose_f(dset_id, error) !Close DATASET
!     CALL h5sclose_f(dspace_id, error) !Close data SPACE
     !-----------------------------------

     !Write XDMF entries for coords:
     write(13,*) '     <Geometry GeometryType="X_Y">'
     write(13,104) '       <DataItem Dimensions="',qy+1,' ',qx+1,'" NumberType="Float" Precision="4" Format="HDF">'
     write(13,*) '        ',trim(fhdf5),':/x'
     write(13,*) '       </DataItem>'
     write(13,104) '       <DataItem Dimensions="',qy+1,' ',qx+1,'" NumberType="Float" Precision="4" Format="HDF">'
     write(13,*) '        ',trim(fhdf5),':/y'
     write(13,*) '       </DataItem>'
     !write(13,104) '       <DataItem Dimensions="',qy+1,' ',qx+1,'" NumberType="Float" Precision="4" Format="HDF">'
     !write(13,*) '        ',fhdf5,':/z'
     !write(13,*) '       </DataItem>'
     write(13,*) '     </Geometry>'

     !=====Read & write scalar arrays in a loop:====

     !---------READ BINDATA:------------
     !density velx vely velz energy press temp gam1 gam2 enuc1 enuc2 kdiff fradx 0001 0002 0003 0004 0005 0006 0007
     !REC#= 1  2    3    4     5     6     7    8    9    10     11   12    13   14(H1) 15(He4)

!   h1   1   0 14
!  he4   2   2 15
!  c12   6   6 16
!  n14   7   7 17
!  o16   8   8 18
! ne20  10  10 19
! mg24  12  12 20

!oburn:
!densi           1
! velx            2
! vely            3
! velz            4
! energ           5
! press           6
! temp            7
! gam1            8
! gam2            9
! enuc1          10
! enuc2          11

! 0001           12 n
! 0002           13 p
! 0003           14 He4
! 0004           15 C12
! 0005           16 O16
! 0006           17 Ne20
! 0007           18
! 0008           19
! 0009           20
! 0010           21
! 0011           22
! 0012           23

!    n   0   1
!    p   1   0
!  he4   2   2
!  c12   6   6
!  o16   8   8
! ne20  10  10

!-------MOVE THIS TO INPUT FILE--------
     !Choose data to read/write:
!     ndat = 7 !36 vars in oburn models. In order of pref, so can cut down size of HDF file if needed.
     !Some early PROMPI runs have less vars output?

!     idat(1) = 1 !rho
!     idat(2) = 2 !velx
!     idat(3) = 3 !vely
!     idat(4) = 4 !velz
!     idat(5) = 16 !O16
!     idat(6) = 6 !press
!     idat(7) = 7 !temp

     !idat(6) = 17 !N14
!     idat(7) = 15!C12
     !idat(8) = 15 !He4
     !idat(9) = 14 !H1
!     idat(10) = 10 !enuc
!     idat(12) = 5 !energy
!     idat(14) = 13 !
!     idat(15)  = 8 !gam1
!     idat(16)  = 9 !gam2
!     idat(17) = 11 !enuc2
!     idat(18) = 12 !
!     idat(19) = 19 !Ne20
!     idat(20) = 20 !Mg24

!     nextra = 3
!     ndattot = ndat+nextra

!Derived extras:
!     datname(50) = "rhohav"  !extras from 50
!     idat(50) = 1            !Need record number for each.
!     datname(51) = "thave"
!     idat(51) = 7
!     datname(52) = "phave"
!     idat(52) = 6
!     datname(53) = "vmag" !takes 3*memory, try in Visit.

!Raw data:
!     datname(1) = "rho" !Can use any names.
!     datname(2) = "velx"
!     datname(3) = "vely"
!     datname(4) = "velz"
!     datname(5) = "O16"
!     datname(6) = "press"
!     datname(7) = "temp"

!     datname(5) = "O16"
!     datname(6) = "N14"
!     datname(7) = "C12"
!     datname(8) = "He4"
!     datname(9) = "H1"
!     datname(10) = "enuc"
!     datname(11) = "press"
!     datname(12) = "energy"
!     datname(13) = "temp"
!     datname(14) = ""
!     datname(15) = "gam1"
!     datname(16) = "gam2"
!     datname(17) = "enuc2"
!     datname(18) = ""
!     datname(19) = "Ne20"
!     datname(20) = "Mg24"
!---------------------------------------

     ndattot = nraw + nderiv
     print*,'Total vars to output = ',ndattot, nraw, nderiv

     nmax = qx*qy
     irecl = nmax*4 !qx !=nmax. Reduced recl since int*4 can't handle such a big number. Now read in qz-sized chunks.
                !Note that sometimes need extra *4 when float_recl = 4 in Prompi (full of zeros on NCI), compiler dependent?
     open(11,file=fbin,STATUS='OLD',ACCESS='DIRECT',RECL=irecl)
     print*,'Opened bindata file:',fbin
     print*,'Using bindat record length = ',irecl

     do i=1,ndattot !Loop over number of raw + derived vars to output

!-------Read raw vars:
        ivar = 0
        if(i.le.nraw) then
           do jj = 1, nvar
              if(varraw(i).eq.varnames(jj)) then
                 dsetname = varnames(jj)
                 ivar = jj
                 print*,'Found var: ',dsetname
                 goto 900
              endif
           enddo

900        continue
           if(ivar.eq.0) then
              print*,"Raw var: Didn't find var!, STOP:",varraw(i),jj
              STOP
           endif

           !Now read, NOT in smallish chunks to save memory:
           print*,'Reading data for:',dsetname,' which has index #',ivar

           read(11,rec=ivar) bindat

!           do j=1,qz
!              do k=1,qy
!                 cnt = cnt+1
!                 read(11,rec=cnt) (bindat(l,k,j),l=1,qx) !read this var in qx chunks. !rho is first.
!              enddo
!           enddo
        endif
!----End read raw vars


!-------Derived vars:
        ivar = 0
        if(i.gt.nraw) then !Just calcs horizontal averages for now.
           !print*,'Searching for derived var: ',varderiv(i-nraw)

           if(i.le.ndattot) then
              do jj = 1, nvar
                 !print*,'jj=',jj
                 if(varderiv(i-nraw).eq.varnames(jj)) then
                    ivar = jj
                    dsetname = varnames(jj)
                    print*,'Found var: ',varnames(jj),' which has index #',ivar
                    dsetname = trim(dsetname) // trim('hav')
                    print*,'New var name = ',dsetname
                    goto 901
                 endif
              enddo

              if(ivar.eq.0) then
                 print*,"Derived var: Didn't find var!, STOP:",varraw(i-nraw),jj
                 STOP
              endif

901           continue
              print*,'Calculating h_ave for: ',dsetname

           read(11,rec=ivar) bindat

!              cnt = (ivar-1)*qy*qz !starting record, each var read in qx records,
!              do j=1,qz
!                 do k=1,qy
!                    cnt = cnt+1
                    !qy*qz reads of qx-sized data chunks.
!                    read(11,rec=ivar) (bindat(l,k,j),l=1,qx) !read this var in qx chunks.
!                 enddo
!              enddo

              do j=1,qx !Average at each r
                 bindat(j,:) = hav(j,qx,qy,bindat)
              enddo
           endif
        endif
!-------End read & calcs for h-averaged vars.

!Sanity checks:
        print*,'Prompi data read:',dsetname,'=',bindat(1,1),'at 1,1,1'
        print*,'Prompi data read:',dsetname,'=',bindat(2,1),'at 2,1,1'
        print*,'Prompi data read:',dsetname,'=',bindat(3,1),'at 3,1,1'
        print*,'Prompi data read:',dsetname,'=',bindat(qx-2,1),'at qx-2,1,1'
        print*,'Prompi data read:',dsetname,'=',bindat(qx-1,1),'at qx-1,1,1'
        print*,'Prompi data read:',dsetname,'=',bindat(qx,1),'at qx,1,1'
        print*,'-------------------'

!------START HDF5/XDMF:

        !dims(1) = qz
        dims(1) = qy
        dims(2) = qx

        CALL h5screate_simple_f(rank, dims, dspace_id, error) !Create data SPACE
        CALL h5dcreate_f(file_id, TRIM(dsetname),  H5T_NATIVE_REAL , dspace_id, dset_id, error) !Create dataset
        CALL h5dwrite_f(dset_id, H5T_NATIVE_REAL, bindat, dims, error) !WRITE data set
        CALL h5dclose_f(dset_id, error) !Close DATA set
        CALL h5sclose_f(dspace_id, error) !Close data SPACE

        !Write XDMF entry for this scalar data:
        write(13,*) '     <Attribute Name="',TRIM(dsetname),'" AttributeType="Scalar" Center="Cell">'
        write(13,104) '       <DataItem Dimensions="',qy,' ',qx,'" NumberType="Float" Precision="4" Format="HDF">'
        write(13,*) '        ',trim(fhdf5),':/',TRIM(dsetname)
        write(13,*) '       </DataItem>'
        write(13,*) '     </Attribute>'

     enddo !End loop over number or vars to output.

     CALL h5fclose_f(file_id, error) !Close HDF5 FILE
     print*,'Closed HDF5 file.'
     print*,'HDF file size estimate (Cartesian coords) = ',(3*(qx*qy*4) + ndattot*(qx*qy*4))/1e9,' GB'
     CALL h5close_f(error)  ! Close FORTRAN interface.

     !Finalise XDMF file:
     write(13,*) '   </Grid>'
     write(13,*) ' </Domain>'
     write(13,*) '</Xdmf>'
     close(13)
     print*,'Closed XML/XDMF file'

     close(11)
     print*,'Closed Prompi bindata file.'
     print*,'NOTE: If the numbers look strange you might need an extra *4 on '
!    &    irecl when float_recl = 4 in Prompi. Compiler-dependent record lengths..'

  enddo !End loop on list of input files.

104  format(a,i4,a,i4,a,i4,a)
105  format(a)

END PROGRAM prompi2hdf5_2D


!===============================================
      real*4 function hav(i,qqx,qqy,dat)

      implicit none

      integer i,j,k
      integer*8 qqx,qqy
      real*8 sum
      real*4 dat(qqx,qqy)

      sum = 0.0d0
      do j=1,qqy                !theta
         !do k=1,qqz             !phi
            sum = sum + dat(i,j)
         !enddo
      enddo
      hav = sum/(qqy)
      RETURN
      END
!================================================
