#!/bin/sh

#
# Copy PROMPI output from current directory to another scratch directory named
# 2020_09_15_14_24_24_some_suffix
#
# Usage
# -----
#
# ./move_prompi_output.sh suffix
#


if [ -z "$1" ]; then
  suffix=""
else
  suffix="_$1"
fi

date=$(date '+%Y_%m_%d_%H_%M_%S')
output_dir="/scratch/ew6/evgenyneu/prompi_output/${date}${suffix}"
mkdir -p $output_dir

mv *.bindata *.header *.ransdat *.ranshead *.logfile.00000_no_time8* $output_dir
echo "Copied output to ${output_dir}"

# Archive to a group directory
archive_dir=/group/ew6/evgenyneu/prompi_output
mkdir -p $archive_dir
archive_path="${archive_dir}/${date}${suffix}.tar"
tar -cf $archive_path -C $output_dir .
echo "Archived output to ${archive_path}"

# Cleanup
rm -f run_complete_file
