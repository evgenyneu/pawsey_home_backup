if [ -f /etc/redhat-release ]; then
    if [ -f ~/.bashrc ]; then
	    . ~/.bashrc
    fi
fi

# display current git branch in prompt
source ~/script/git-prompt.sh
PS1='\W$(__git_ps1 " (%s)") \$ '
# unstaged (*) and staged (+) changes will be shown next to the branch name
export GIT_PS1_SHOWDIRTYSTATE=true
# shows if there're untracked files
export GIT_PS1_SHOWUNTRACKEDFILES=true


# PROMPI  (https://github.com/evgenyneu/PROMPI)
# -------

# makefile location: root/sites/$SITE/makefile.local
export SITE=magnus

# Source code
export PROMPI_SRC=$MYSCRATCH/PROMPI/root/src

# Aliases
# -------

# CD to my current PROMPI directory
alias 2d='cd $MYSCRATCH/PROMPI/setups/ccp_two_layers/2d'
alias 3d='cd $MYSCRATCH/PROMPI/setups/ccp_two_layers/3d'

# Computing balance on pawsey
alias balance='pawseyAccountBalance -p ew6 -users'
